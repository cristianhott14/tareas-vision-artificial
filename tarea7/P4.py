import cv2 as cv
import numpy as np
import time
from backpropagation import* #Incorpora clase con Backpropagation

ini = time.time()
np.set_printoptions(threshold=np.inf) #Aumentar el rango de visualización de las matrices

#Imagenes de 1s
img1 = cv.imread("images/1.png")
img1 = cv.cvtColor(img1,cv.COLOR_BGR2GRAY) #Transforma a escala de grises
ret1, img1 = cv.threshold(img1,250,1,0) #Binariza la imagen
img1_fil = np.array(img1.reshape(-1)) #Transforma la matriz de 10 x 10 a 1 x 100
img1_col = np.transpose([img1_fil])       #Transforma la matriz de 1 x 100 a 100 x 1
#print(np.shape(img1_fil))
#print(img1_col )

img1_1 = cv.imread("images/1_1.png")
img1_1 = cv.cvtColor(img1_1,cv.COLOR_BGR2GRAY)
ret1_1, img1_1 = cv.threshold(img1_1,250,1,0) 
img1_1_fil = np.array(img1_1.reshape(-1)) 
img1_1_col = np.transpose([img1_1_fil])    

img1_2 = cv.imread("images/1_2.png")
img1_2 = cv.cvtColor(img1_2,cv.COLOR_BGR2GRAY)
ret1_2, img1_2 = cv.threshold(img1_2,250,1,0)  
img1_2_fil = np.array(img1_2.reshape(-1)) 
img1_2_col = np.transpose([img1_2_fil])    

img1_3 = cv.imread("images/1_3.png")
img1_3 = cv.cvtColor(img1_3,cv.COLOR_BGR2GRAY)
ret1_3, img1_3 = cv.threshold(img1_3,250,1,0)  
img1_3_fil = np.array(img1_3.reshape(-1)) 
img1_3_col = np.transpose([img1_3_fil])  

img1_4 = cv.imread("images/1_4.png")
img1_4 = cv.cvtColor(img1_4,cv.COLOR_BGR2GRAY)
ret1_4, img1_4 = cv.threshold(img1_4,250,1,0)  
img1_4_fil = np.array(img1_4.reshape(-1)) 
img1_4_col = np.transpose([img1_4_fil])  

img1_5 = cv.imread("images/1_5.png")
img1_5 = cv.cvtColor(img1_5,cv.COLOR_BGR2GRAY) 
ret1_5, img1_5 = cv.threshold(img1_5,250,1,0) 
img1_5_fil = np.array(img1_5.reshape(-1)) 
img1_5_col = np.transpose([img1_5_fil])  

#Imagenes de 2s
img2 = cv.imread("images/2.png")
img2 = cv.cvtColor(img2,cv.COLOR_BGR2GRAY) #Transforma a escala de grises
ret2, img2 = cv.threshold(img2,250,1,0) #Binariza la imagen
img2_fil = np.array(img2.reshape(-1)) #Transforma la matriz de 10 x 10 a 1 x 100
img2_col = np.transpose([img2_fil])       #Transforma la matriz de 1 x 100 a 100 x 1

img2_1 = cv.imread("images/2_1.png")
img2_1 = cv.cvtColor(img2_1,cv.COLOR_BGR2GRAY) 
ret2_1, img2_1 = cv.threshold(img2_1,250,1,0)
img2_1_fil = np.array(img2_1.reshape(-1)) 
img2_1_col = np.transpose([img2_1_fil])    

img2_2 = cv.imread("images/2_2.png")
img2_2 = cv.cvtColor(img2_2,cv.COLOR_BGR2GRAY) 
ret2_2, img2_2 = cv.threshold(img2_2,250,1,0)
img2_2_fil = np.array(img2_2.reshape(-1)) 
img2_2_col = np.transpose([img2_2_fil])    

img2_3 = cv.imread("images/2_3.png")
img2_3 = cv.cvtColor(img2_3,cv.COLOR_BGR2GRAY) 
ret2_3, img2_3 = cv.threshold(img2_3,250,1,0)
img2_3_fil = np.array(img2_3.reshape(-1)) 
img2_3_col = np.transpose([img2_3_fil])  

img2_4 = cv.imread("images/2_4.png")
img2_4 = cv.cvtColor(img2_4,cv.COLOR_BGR2GRAY) 
ret2_4, img2_4 = cv.threshold(img2_4,250,1,0)
img2_4_fil = np.array(img2_4.reshape(-1)) 
img2_4_col = np.transpose([img2_4_fil])  

img2_5 = cv.imread("images/2_5.png")
img2_5 = cv.cvtColor(img2_5,cv.COLOR_BGR2GRAY) 
ret2_5, img2_5 = cv.threshold(img2_5,250,1,0)
img2_5_fil = np.array(img2_5.reshape(-1)) 
img2_5_col = np.transpose([img2_5_fil])  

#Imagenes de 3s
img3 = cv.imread("images/3.png")
img3 = cv.cvtColor(img3,cv.COLOR_BGR2GRAY) #Transforma a escala de grises
ret3, img3 = cv.threshold(img3,250,1,0) #Binariza la imagen
img3_fil = np.array(img3.reshape(-1)) #Transforma la matriz de 10 x 10 a 1 x 100
img3_col = np.transpose([img3_fil])       #Transforma la matriz de 1 x 100 a 100 x 1

img3_1 = cv.imread("images/3_1.png")
img3_1 = cv.cvtColor(img3_1,cv.COLOR_BGR2GRAY) 
ret3_1, img3_1 = cv.threshold(img3_1,250,1,0)
img3_1_fil = np.array(img3_1.reshape(-1)) 
img3_1_col = np.transpose([img3_1_fil])    

img3_2 = cv.imread("images/3_2.png")
img3_2 = cv.cvtColor(img3_2,cv.COLOR_BGR2GRAY) 
ret3_2, img3_2 = cv.threshold(img3_2,250,1,0)
img3_2_fil = np.array(img3_2.reshape(-1)) 
img3_2_col = np.transpose([img3_2_fil])    

img3_3 = cv.imread("images/3_3.png")
img3_3 = cv.cvtColor(img3_3,cv.COLOR_BGR2GRAY) 
ret3_3, img3_3 = cv.threshold(img3_3,250,1,0)
img3_3_fil = np.array(img3_3.reshape(-1)) 
img3_3_col = np.transpose([img3_3_fil])  

img3_4 = cv.imread("images/3_4.png")
img3_4 = cv.cvtColor(img3_4,cv.COLOR_BGR2GRAY) 
ret3_4, img3_4 = cv.threshold(img3_4,250,1,0)
img3_4_fil = np.array(img3_4.reshape(-1)) 
img3_4_col = np.transpose([img3_4_fil])  

img3_5 = cv.imread("images/3_5.png")
img3_5 = cv.cvtColor(img3_5,cv.COLOR_BGR2GRAY) 
ret3_5, img3_5 = cv.threshold(img3_5,250,1,0)
img3_5_fil = np.array(img3_5.reshape(-1)) 
img3_5_col = np.transpose([img3_5_fil])  

#Imagenes de 4s
img4 = cv.imread("images/4.png")
img4 = cv.cvtColor(img4,cv.COLOR_BGR2GRAY) #Transforma a escala de grises
ret4, img4 = cv.threshold(img4,250,1,0) #Binariza la imagen
img4_fil = np.array(img4.reshape(-1)) #Transforma la matriz de 10 x 10 a 1 x 100
img4_col = np.transpose([img4_fil])       #Transforma la matriz de 1 x 100 a 100 x 1

img4_1 = cv.imread("images/4_1.png")
img4_1 = cv.cvtColor(img4_1,cv.COLOR_BGR2GRAY) 
ret4_1, img4_1 = cv.threshold(img4_1,250,1,0)
img4_1_fil = np.array(img4_1.reshape(-1)) 
img4_1_col = np.transpose([img4_1_fil])    

img4_2 = cv.imread("images/4_2.png")
img4_2 = cv.cvtColor(img4_2,cv.COLOR_BGR2GRAY) 
ret4_2, img4_2 = cv.threshold(img4_2,250,1,0)
img4_2_fil = np.array(img4_2.reshape(-1)) 
img4_2_col = np.transpose([img4_2_fil])    

img4_3 = cv.imread("images/4_3.png")
img4_3 = cv.cvtColor(img4_3,cv.COLOR_BGR2GRAY) 
ret4_3, img4_3 = cv.threshold(img4_3,250,1,0)
img4_3_fil = np.array(img4_3.reshape(-1)) 
img4_3_col = np.transpose([img4_3_fil])  

img4_4 = cv.imread("images/4_4.png")
img4_4 = cv.cvtColor(img4_4,cv.COLOR_BGR2GRAY) 
ret4_4, img4_4 = cv.threshold(img4_4,250,1,0)
img4_4_fil = np.array(img4_4.reshape(-1)) 
img4_4_col = np.transpose([img4_4_fil])  

img4_5 = cv.imread("images/4_5.png")
img4_5 = cv.cvtColor(img4_5,cv.COLOR_BGR2GRAY) 
ret4_5, img4_5 = cv.threshold(img4_5,250,1,0)
img4_5_fil = np.array(img4_5.reshape(-1)) 
img4_5_col = np.transpose([img4_5_fil])  

# Parámetros de la red
#[R S1 S2 S3] Agregar elementos según número de capas deseado
#Agregar una función de transferencia por cada capa de la red 1:sigmoidal, 2:tanh,  3:lineal.
red = Backprop([100, 4, 3, 1], [1, 2, 3])

red.alfa = 0.01 # tasa de aprendizaje de la red
red.num_epochs = 10000 # número máximo de épocas de entrenemiento
red.target_error = 1E-6 # Error objetivo de entrenmiento

# Entrenamiento
# Matriz 20x100 contiene los 20 vectores filas de las 20 imagenes distorsionadas
P = np.array([img1_1_fil,img1_2_fil,img1_3_fil,img1_4_fil,img1_5_fil,
             img2_1_fil,img2_2_fil,img2_3_fil,img2_4_fil,img2_5_fil,
             img3_1_fil,img3_2_fil,img3_3_fil,img3_4_fil,img3_5_fil,
             img4_1_fil,img4_2_fil,img4_3_fil,img4_4_fil,img4_5_fil])

# Matriz de entrada para entrenamiento de 100x20
P=np.transpose(P)       #Se utiliza la traspuesta de P

# Vector de salida T
T = np.array([[1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4]])

# Inicializa pesos y ganancias
red.W_init(0)
red.B_init(0)

# Entrena la red neuronal
red.train(P, T) 

print("Pesos:") # Despliega en pantalla los valores de los pesos de la red.
print(red.W)
print("Ganancias:")  # Despliega en pantalla los valores de las ganancias de la red.
print(red.B)

print("\n")
print("La salida de la red para la matriz de entrada P (traspuesta) es: ")
print(red.sim(P))

print("\n")
print("La salida de la red para entrada con imagen 1 sin distorsión es ",red.sim(img1_col)) # Despliega en pantalla la salida de la red para la entrada
print("La salida de la red para entrada con imagen 2 sin distorsión es ",red.sim(img2_col))
print("La salida de la red para entrada con imagen 3 sin distorsión es ",red.sim(img3_col))
print("La salida de la red para entrada con imagen 4 sin distorsión es ",red.sim(img4_col)) 

fin = time.time()
print("Tiempo transcurrido:",(fin-ini)) 
