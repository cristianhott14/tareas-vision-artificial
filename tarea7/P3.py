from backpropagation import* #Incorpora clase con Backpropagation
import math # Sólo se utilizó para incorporar función seno y pi

# Parámetros de la red
# [R S1 S2] 
# Agregar una función de transferencia por cada capa de la red 1:sigmoidal, 2:tanh,  3:lineal.
red2 = Backprop([1, 2, 1], [2, 3])

red2.alfa = 0.1 # tasa de aprendizaje de la red
red2.num_epochs = 5000 # número máximo de épocas de entrenemiento
red2.target_error = 1E-5 # Error objetivo de entrenmiento

# Ejemplos de entrenamiento
P = np.array([[-2, -1.2, -0.4, 0.4, 1.2, 2]]) # Vector de entrada (Debe ser un arreglo 2D, por eso se utilizo doble paréntesis cuadrado)
T = np.empty((1,6)) # El arreglo T contiene las salidas esperadas para las correspondientes entradas P
for k in range(len(P[0,:])):
  T[0][k] = math.sin(P[0,k]*math.pi/4) # Llena el vector de salida para este ejemplo

# Valores iniciales de pesos y ganancias

red2.W[1] = np.array([[0.75], [0.75]])
red2.W[2] = np.array([[0.6, 0.6]])
red2.B[1] = np.array([[0.001], [0.001]])
red2.B[2] = np.array([[-0.001]])


red2.train(P, T) # entrena la red neuronal

print("------------------------------------------------------")
print("Pesos:",red2.W) # Despliega en pantalla los valores de los pesos de la red.
print("Ganancias:",red2.B)  # Despliega en pantalla los valores de las ganancias de la red.
print("------------------------------------------------------")

#Calcula para la función evaluado en 0.2
a=math.sin(math.pi/4)*0.2
#Se despliega la salida de la red para la entrada 0.2
print("Salida de la red para 0.2:", red2.sim(0.2))
print("Función evaluado en 0.2: ", a)
