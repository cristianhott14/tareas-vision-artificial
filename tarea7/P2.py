from backpropagation import* #Incorpora clase con Backpropagation

# Parámetros de la red
# [R S1 S2 S3] 
# Agregar una función de transferencia por cada capa de la red 1:sigmoidal, 2:tanh,  3:lineal.
red2 = Backprop([9, 3, 2, 1], [1, 2, 3])

red2.alfa = 0.001 # tasa de aprendizaje de la red
red2.num_epochs = 5000 # número máximo de épocas de entrenemiento
red2.target_error = 1E-15 # Error objetivo de entrenmiento

# Ejemplos de entrenamiento
P = np.array([[1,-1,1,-1,1,-1,1,-1,1],[2,-2,2,-2,2,-2,2,-2,2],[3,-3,3,-3,3,-3,3,-3,3],[4,-4,4,-4,4,-4,4,-4,4]]) # Vector de entrada (Debe ser un arreglo 2D, por eso se utilizo doble paréntesis cuadrado)
P = np.transpose(P) #Traspuesta de la matriz P quedando como 9x4
T = np.array([[1],[2],[3],[4]]) #Vector de salida 4x1

# Valores iniciales de pesos y ganancias
Wa=0.41
Wb=0.29
red2.W[1] = np.array([[Wa,Wb,Wa,Wb,Wa,Wb,Wa,Wb,Wa],[Wa,Wb,Wa,Wb,Wa,Wb,Wa,Wb,Wa],[Wa,Wb,Wa,Wb,Wa,Wb,Wa,Wb,Wa]])
Wc=0.5
red2.W[2] = np.array([[Wc,Wc,Wc],[Wc,Wc,Wc]])
Wd=2
red2.W[3] = np.array([[Wd, Wd]])

Ba=-1.799
red2.B[1] = np.array([[Ba], [Ba], [Ba]])
Bb=-1.399
red2.B[2] = np.array([[Bb], [Bb]])
Bc=3.7
red2.B[3] = np.array([[Bc]])


red2.train(P, T) # entrena la red neuronal

print("Pesos:",red2.W) # Despliega en pantalla los valores de los pesos de la red.
print("Ganancias:",red2.B)  # Despliega en pantalla los valores de las ganancias de la red.

#Vectores columnas de entrada por separado
X1=np.array([[1,-1,1,-1,1,-1,1,-1,1]])
X1=X1.transpose()
X2=np.array([[2,-2,2,-2,2,-2,2,-2,2]])
X2=X2.transpose()
X3=np.array([[3,-3,3,-3,3,-3,3,-3,3]])
X3=X3.transpose()
X4=np.array([[4,-4,4,-4,4,-4,4,-4,4]])
X4=X4.transpose()

#print("La salida de la red para entrada X1 es ", red2.sim(P))
print("La salida de la red para entrada 1 es ", red2.sim(X1)) # Despliega en pantalla la salida de la red para una entrada X1
print("La salida de la red para entrada 2 es ", red2.sim(X2)) # Despliega en pantalla la salida de la red para una entrada X2
print("La salida de la red para entrada 3 es ", red2.sim(X3)) # Despliega en pantalla la salida de la red para una entrada X3
print("La salida de la red para entrada 4 es ", red2.sim(X4)) # Despliega en pantalla la salida de la red para una entrada X4