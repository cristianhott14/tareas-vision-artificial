import cv2

#Leer imagen
img=cv2.imread('foto1.jpg')

#Convertir de BGR a HSV
img_HSV=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

#Se separa los canales de imagen en H, S y V con el comando "cv2.split"
H,S,V=cv2.split(img_HSV)

#Solo se muestra el canal V y no los canales que contiene la tonalidad y saturación
cv2.imshow('P4 Imagen en escala de grises',V)
cv2.waitKey(0)
cv2.destroyAllWindows()