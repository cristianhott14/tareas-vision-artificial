import cv2

#Leer imagen
img=cv2.imread('foto1.jpg')

#Convertir de BGR a RGB
img_org=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

#Crea una copia de la imagen
img_mod=img_org.copy()

#Obtener información de la cantidad de columnas como ancho
# y el total de filas como alto
ancho=img_mod.shape[0]
alto=img_mod.shape[1]

#Se realiza un recorrido sobre la imagen
for fil in range(ancho):
    for col in range(alto):
        #Se realizan los cambios por canal de la imagen con las combinaciones que se difinio en la tabla
        # 000 a 111
        if img_org[fil,col,0] < 120 and img_org[fil,col,1] < 120 and img_org[fil,col,2] < 120:
            img_mod[fil,col,0]=255
            img_mod[fil,col,1]=255
            img_mod[fil,col,2]=255
        # 001 a 010
        if img_org[fil,col,0] < 120 and img_org[fil,col,1] < 120 and img_org[fil,col,2] >= 120:
            img_mod[fil,col,0]=0
            img_mod[fil,col,1]=255
            img_mod[fil,col,2]=0  
        # 010 a 110
        if img_org[fil,col,0] < 120 and img_org[fil,col,1] >= 120 and img_org[fil,col,2] < 120:
            img_mod[fil,col,0]=255
            img_mod[fil,col,1]=255
            img_mod[fil,col,2]=0
        # 011 a 100
        if img_org[fil,col,0] < 120 and img_org[fil,col,1] >= 120 and img_org[fil,col,2] >= 120:
            img_mod[fil,col,0]=255
            img_mod[fil,col,1]=0
            img_mod[fil,col,2]=0
        # 100 a 011
        if img_org[fil,col,0] >= 120 and img_org[fil,col,1] < 120 and img_org[fil,col,2] < 120:
            img_mod[fil,col,0]=0
            img_mod[fil,col,1]=255
            img_mod[fil,col,2]=255
        # 101 a 001
        if img_org[fil,col,0] >= 120 and img_org[fil,col,1] < 120 and img_org[fil,col,2] >= 120:
            img_mod[fil,col,0]=0
            img_mod[fil,col,1]=0
            img_mod[fil,col,2]=255
        # 110 a 101
        if img_org[fil,col,0] >= 120 and img_org[fil,col,1] >= 120 and img_org[fil,col,2] < 120:
            img_mod[fil,col,0]=255
            img_mod[fil,col,1]=0
            img_mod[fil,col,2]=255
        # 111 a 000
        if img_org[fil,col,0] >= 120 and img_org[fil,col,1] >= 120 and img_org[fil,col,2] >= 120:
            img_mod[fil,col,0]=0
            img_mod[fil,col,1]=0
            img_mod[fil,col,2]=0

#Convertir la imagen modificada de RGB a BGR
img_mod2=cv2.cvtColor(img_mod,cv2.COLOR_RGB2BGR)

#Mostrar imagen modificada y cerrar al presionar alguna tecla
cv2.imshow('P1',img_mod2)
cv2.waitKey(0)
cv2.destroyAllWindows()