import cv2

#Leer imagen
img=cv2.imread('foto1.jpg')

#Convertir de BGR a escala de grises
img_gris=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

#Crea una copia de la imagen para cada escala
escala_rojo=img.copy()
escala_verde=img.copy()
escala_azul=img.copy()
escala_cyan=img.copy()
escala_magenta=img.copy()
escala_amarillo=img.copy()

#Se trabaja solo con los canales BGR que corresponda y se le asigna la escala de grises.
escala_rojo[:,:,0] = 0
escala_rojo[:,:,1] = 0
escala_rojo[:,:,2] = img_gris       #se asigna la esacala de grises al canal R

escala_verde[:,:,0] = 0
escala_verde[:,:,1] = img_gris      #se asigna la esacala de grises al canal G
escala_verde[:,:,2] = 0

escala_azul[:,:,0] = img_gris       #se asigna la esacala de grises al canal B
escala_azul[:,:,1] = 0
escala_azul[:,:,2] = 0

escala_cyan[:,:,0] = img_gris       #se asigna la esacala de grises al canal B
escala_cyan[:,:,1] = img_gris       #se asigna la esacala de grises al canal G
escala_cyan[:,:,2] = 0

escala_magenta[:,:,0] = img_gris    #se asigna la esacala de grises al canal B
escala_magenta[:,:,1] = 0
escala_magenta[:,:,2] = img_gris    #se asigna la esacala de grises al canal R

escala_amarillo[:,:,0] = 0
escala_amarillo[:,:,1] = img_gris   #se asigna la esacala de grises al canal G
escala_amarillo[:,:,2] = img_gris   #se asigna la esacala de grises al canal R

#Mostrar imagenes en sus respectivas escalas y se cierra al presionar alguna tecla
cv2.imshow('P3. Escala de rojos',escala_rojo)
cv2.imshow('P3. Escala de verde',escala_verde)
cv2.imshow('P3. Escala de azul',escala_azul)
cv2.imshow('P3. Escala de cyan',escala_cyan)
cv2.imshow('P3. Escala de magenta',escala_magenta)
cv2.imshow('P3. Escala de amarillo',escala_amarillo)
cv2.waitKey(0)
cv2.destroyAllWindows()