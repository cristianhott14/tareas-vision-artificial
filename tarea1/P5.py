import cv2

#Leer imagen
img=cv2.imread('foto1.jpg')

#Convertir de BGR a escala de grises
img_gris=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

#Crea una copia de cada imagen
img_mod=img.copy()
img_mod2=img_gris.copy()

#Imagen en negativo: restando 255 menos la imagen original 
img_mod=255-img

#Imagen en negativo de escala de grises: restando 255 menos la imagen en escala de grises
img_mod2=255-img_gris

#Concatenar imagenes de manera horizonal con el comando "cv2.hconcat"
negativos_rgb=cv2.hconcat([img,img_mod])
negativos_grises=cv2.hconcat([img_gris,img_mod2])

#Mostrar imagenes modificadas y cerrar al presionar alguna tecla
cv2.imshow('P5 Imagen en negativo RGB',negativos_rgb)
cv2.imshow('P5 Imagen en negativo escala de grises',negativos_grises)
cv2.waitKey(0)
cv2.destroyAllWindows()