import cv2
import numpy as np

#Leer imagen
img=cv2.imread('foto1.jpg')

#Convertir de BGR a RGB
img_rgb=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

#Obtener información de la cantidad de columnas como ancho,
# el total de filas como alto y los canales RGB
ancho=img_rgb.shape[0]
alto=img_rgb.shape[1]
canal=img_rgb.shape[2]
resolucion = alto*ancho #resolución de la imagen

#Se crea la matríz de ceros de 256 filas x 4 columnas
matriz = np.zeros((256,4),int)           
#Se rellena la primera columna desde 0 a 255
matriz[:,0] = list(range(256))  

#Se realiza un recorrido sobre la imagen por filas, columnas y canales
for fil in range(ancho):
    for col in range(alto):
        for canl in range(canal):
            if canl in matriz :                             
                matriz[img_rgb[fil,col,canl],canl+1] += 1       #en caso de que el nivel de 0 a 255 correspondiente al canal este presente, se sumara 1
            else:
                matriz[img_rgb[fil,col,canl],canl+1] = 0        #en caso contrario se sumara 0

#Se imprime la matriz completa de 256 x 4
with np.printoptions(threshold=np.inf):                      
    print(matriz)

#Suma total de pixeles por canal
Tpixeles_R = sum(matriz[:,1])
Tpixeles_G = sum(matriz[:,2])
Tpixeles_B = sum(matriz[:,3])

#Entrega mensaje en pantalla si el conteo de pixeles por canal corresponde con el total de pixeles de la imagen
if resolucion == Tpixeles_R == Tpixeles_G == Tpixeles_B:
    print("Los números de pixeles por canal") 
    print("Canal R:", Tpixeles_R)
    print("Canal G:", Tpixeles_G)
    print("Canal B:", Tpixeles_B)
    print("Coinciden con los números de pixeles totales:", resolucion)
else:
    print("El numero de pixeles no coinciden")