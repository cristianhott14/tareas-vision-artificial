import numpy as np
import matplotlib.pyplot as plt
import torch

# Implementación del Perceptrón con PyTorch

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu") # Permite realizar procesamiento en GPU (Graphics Processing Unit)

class Perceptron():
    def __init__(self, num_features):
        self.num_features = num_features
        self.weights = torch.zeros(num_features, 1, 
                                   dtype=torch.float32, device=device)
        self.bias = torch.zeros(1, dtype=torch.float32, device=device)
        
        self.ones = torch.ones(1, device=device)
        self.zeros = torch.zeros(1, device=device)

    def forward(self, x):
        linear = torch.mm(x, self.weights) + self.bias
        predictions = torch.where(linear > 0., self.ones, self.zeros)
        return predictions
        
    def backward(self, x, y):  
        predictions = self.forward(x)
        errors = y - predictions
        return errors
        
    def train(self, x, y, epochs):
        for e in range(epochs):
            
            for i in range(y.shape[0]):
                errors = self.backward(x[i].reshape(1, self.num_features), y[i]).reshape(-1)
                self.weights += (errors * x[i]).reshape(self.num_features, 1)
                self.bias += errors
                
    def evaluate(self, x, y):
        predictions = self.forward(x).reshape(-1)
        accuracy = torch.sum(predictions == y).float() / y.shape[0]
        return accuracy

# Entrenamiento del Perceptrón

ppn = Perceptron(num_features=4) # num_features es el número de componentes del vector de entrada

X_train = torch.tensor([[0,0,0,0],[0,0,0,1],[0,0,1,0],[0,0,1,1],[0,1,0,0],[0,1,0,1],[0,1,1,0],[0,1,1,1],[1,0,0,0],[1,0,0,1],[1,0,1,0],[1,0,1,1]], dtype=torch.float32, device=device) # Ingresar entradas como vector fila. Un tensor en PyTorch es básicamente lo mismo que un array en NumPy.
y_train = torch.tensor([1,1,1,1,0,0,0,1,0,0,0,1], dtype=torch.float32, device=device) # Ingresar salidas deseadas para cada entrada

ppn.train(X_train, y_train, epochs=5) # epochs son el número de iteraciones del algoritmo de entrenamiento (en cada iteración se aplica la regla de aprendizaje a todos los ejemplos de entrenamiento)

print('Parámetros del modelo:\n\n')
print('  Pesos: %s\n' % ppn.weights) # Despliega los pesos del perceptrón
print('  Bias: %s\n' % ppn.bias) # Despliega el bias del perceptrón

# Evaluación del modelo

train_acc = ppn.evaluate(X_train, y_train)
print('Precisión del entrenamiento: %.2f%%' % (train_acc*100)) # Despliega la precisión del entrenamiento

test_data = torch.tensor([[1,1,0,0]], dtype=torch.float32, device=device)
test_data2 = torch.tensor([[1,1,0,1]], dtype=torch.float32, device=device)
test_data3 = torch.tensor([[1,1,1,0]], dtype=torch.float32, device=device)
test_data4 = torch.tensor([[1,1,1,1]], dtype=torch.float32, device=device)
sim = ppn.forward(test_data)
sim2 = ppn.forward(test_data2)
sim3 = ppn.forward(test_data3)
sim4 = ppn.forward(test_data4)
print('Salida particular del perceptrón con entrada [1,1,0,0] : ', sim)
print('Salida particular del perceptrón con entrada [1,1,0,1] : ', sim2)
print('Salida particular del perceptrón con entrada [1,1,1,0] : ', sim3)
print('Salida particular del perceptrón con entrada [1,1,1,1] : ', sim4)