import numpy as np
import matplotlib.pyplot as plt
import torch

# Implementación del Perceptrón con PyTorch

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu") # Permite realizar procesamiento en GPU (Graphics Processing Unit)

class Perceptron():
    def __init__(self, num_features):
        self.num_features = num_features
        self.weights = torch.zeros(num_features, 1, 
                                   dtype=torch.float32, device=device)
        self.bias = torch.zeros(1, dtype=torch.float32, device=device)
        
        self.ones = torch.ones(1, device=device)
        self.zeros = torch.zeros(1, device=device)

    def forward(self, x):
        linear = torch.mm(x, self.weights) + self.bias
        predictions = torch.where(linear > 0., self.ones, self.zeros)
        return predictions
        
    def backward(self, x, y):  
        predictions = self.forward(x)
        errors = y - predictions
        return errors
        
    def train(self, x, y, epochs):
        for e in range(epochs):
            
            for i in range(y.shape[0]):
                errors = self.backward(x[i].reshape(1, self.num_features), y[i]).reshape(-1)
                self.weights += (errors * x[i]).reshape(self.num_features, 1)
                self.bias += errors
                
    def evaluate(self, x, y):
        predictions = self.forward(x).reshape(-1)
        accuracy = torch.sum(predictions == y).float() / y.shape[0]
        return accuracy

# Entrenamiento de los perceptrones

ppn_and = Perceptron(num_features=2) # num_features es el número de componentes del vector de entrada
ppn_imp = Perceptron(num_features=2)

x_train = torch.tensor([[0, 0], [0, 1], [1, 0], [1, 1]], dtype=torch.float32, device=device) # Ingresar entradas como vector fila. Un tensor en PyTorch es básicamente lo mismo que un array en NumPy.
# Fucnión and
y_train_and = torch.tensor([0, 0, 0, 1], dtype=torch.float32, device=device) # Ingresar salidas deseadas para cada entrada
# Fucnión implica
y_train_imp = torch.tensor([1, 1, 0, 1], dtype=torch.float32, device=device)

ppn_and.train(x_train, y_train_and, epochs=5) # epochs son el número de iteraciones del algoritmo de entrenamiento (en cada iteración se aplica la regla de aprendizaje a todos los ejemplos de entrenamiento)
ppn_imp.train(x_train, y_train_imp, epochs=5)

print("Fucnión AND")
print('Parámetros del modelo:\n\n')
print('  Pesos: %s\n' % ppn_and.weights) # Despliega los pesos del perceptrón
print('  Bias: %s\n' % ppn_and.bias) # Despliega el bias del perceptrón

print("Fucnión IMPLICA ")
print('Parámetros del modelo:\n\n')
print('  Pesos: %s\n' % ppn_imp.weights) # Despliega los pesos del perceptrón
print('  Bias: %s\n' % ppn_imp.bias) # Despliega el bias del perceptrón



# Evaluación del modelo
# Función AND
train_acc_and = ppn_and.evaluate(x_train, y_train_and)
print('Precisión del entrenamiento AND: %.2f%%' % (train_acc_and*100)) # Despliega la precisión del entrenamiento

test_data_and = torch.tensor([[0.99, 1.001]], dtype=torch.float32, device=device)
sim_and = ppn_and.forward(test_data_and)
print('Salida particular del perceptrón AND: ', sim_and)

# Función IMPLICA
train_acc_imp = ppn_imp.evaluate(x_train, y_train_imp)
print('Precisión del entrenamiento IMPLICA: %.2f%%' % (train_acc_imp*100)) # Despliega la precisión del entrenamiento

test_data_imp = torch.tensor([[0.99, 1.001]], dtype=torch.float32, device=device)
sim_imp = ppn_imp.forward(test_data_imp)
print('Salida particular del perceptrón IMPLICA: ', sim_imp)


# Gráfico 2D de recta de decisión

# Función AND
w_and, b_and = ppn_and.weights, ppn_and.bias

x0_min_and = -0.5
x1_min_and = ( (-(w_and[0] * x0_min_and) - b_and[0]) 
          / w_and[1] )

x0_max_and = 1.5
x1_max_and = ( (-(w_and[0] * x0_max_and) - b_and[0]) 
          / w_and[1] )

# Traspasa procesamiento a CPU y transforma tensor PyTorch en array NumPy.
x1_min2_and = x1_min_and.cpu()
x1_min2_and = np.array(x1_min2_and)
x1_max2_and = x1_max_and.cpu()
x1_max2_and = np.array(x1_max2_and)
x_train2_and = x_train.cpu()
x_train2_and = np.array(x_train2_and)
y_train_and2 = y_train_and.cpu()
y_train_and2 = np.array(y_train_and2)

plt.figure()
plt.plot([x0_min_and, x0_max_and], [np.array(x1_min2_and), np.array(x1_max2_and)])
plt.scatter(x_train2_and[y_train_and2==0, 0], x_train2_and[y_train_and2==0, 1], label='class 0', marker='o')
plt.scatter(x_train2_and[y_train_and2==1, 0], x_train2_and[y_train_and2==1, 1], label='class 1', marker='s')

plt.legend(loc='upper right')
plt.title("Función AND con Torch")
plt.grid(True)


# Función IMPLICA

w_imp, b_imp = ppn_imp.weights, ppn_imp.bias

x0_min_imp = -0.5
x1_min_imp = ( (-(w_imp[0] * x0_min_imp) - b_imp[0]) 
          / w_imp[1] )

x0_max_imp = 1.5
x1_max_imp = ( (-(w_imp[0] * x0_max_imp) - b_imp[0]) 
          / w_imp[1] )

# Traspasa procesamiento a CPU y transforma tensor PyTorch en array NumPy.
x1_min2_imp = x1_min_imp.cpu()
x1_min2_imp = np.array(x1_min2_imp)
x1_max2_imp = x1_max_imp.cpu()
x1_max2_imp = np.array(x1_max2_imp)
x_train2_imp = x_train.cpu()
x_train2_imp = np.array(x_train2_imp)
y_train_imp2 = y_train_imp.cpu()
y_train_imp2 = np.array(y_train_imp2)

plt.figure()
plt.plot([x0_min_imp, x0_max_imp], [np.array(x1_min2_imp), np.array(x1_max2_imp)])
plt.scatter(x_train2_imp[y_train_imp2==0, 0], x_train2_imp[y_train_imp2==0, 1], label='class 0', marker='o')
plt.scatter(x_train2_imp[y_train_imp2==1, 0], x_train2_imp[y_train_imp2==1, 1], label='class 1', marker='s')

plt.legend(loc='upper right')
plt.title("Función IMPLICA con Torch")
plt.grid(True)
plt.show()