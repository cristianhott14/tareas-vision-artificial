import numpy as np
import matplotlib.pyplot as plt

# Implementación del Perceptrón con NumPy

class Perceptron():
    def __init__(self, num_features):
        self.num_features = num_features
        self.weights = np.zeros((num_features, 1), dtype=float)
        self.bias = np.zeros(1, dtype=float)

    def forward(self, x):
        linear = np.dot(x, self.weights) + self.bias 
        predictions = np.where(linear > 0., 1, 0)
        return predictions
        
    def backward(self, x, y):  
        predictions = self.forward(x)
        errors = y - predictions
        return errors
        
    def train(self, x, y, epochs):
        for e in range(epochs):
            
            for i in range(y.shape[0]):
                errors = self.backward(x[i].reshape(1, self.num_features), y[i]).reshape(-1)
                self.weights += (errors * x[i]).reshape(self.num_features, 1)
                self.bias += errors
                
    def evaluate(self, x, y):
        predictions = self.forward(x).reshape(-1)
        accuracy = np.sum(predictions == y) / y.shape[0]
        return accuracy


# Entrenamiento de perceptrones
# Función and
ppn_and = Perceptron(num_features=2) # num_features es el número de componentes del vector de entrada
# Función implica
ppn_imp = Perceptron(num_features=2)

x_train = np.array([[0, 0], [0, 1], [1, 0], [1, 1]]) # Ingresar entradas como vector fila

# Salidas función and
y_train_and = np.array([0, 0, 0, 1]) # Ingresar salidas deseadas para cada entrada
# Salidas función implica
y_train_imp = np.array([1, 1, 0, 1]) # Ingresar salidas deseadas para cada entrada

ppn_and.train(x_train, y_train_and, epochs=5) # epochs son el número de iteraciones del algoritmo de entrenamiento (en cada iteración se aplica la regla de aprendizaje a todos los ejemplos de entrenamiento)
ppn_imp.train(x_train, y_train_imp, epochs=5) # epochs son el número de iteraciones del algoritmo de entrenamiento (en cada iteración se aplica la regla de aprendizaje a todos los ejemplos de entrenamiento)

print("Función AND")
print('Parámetros del modelo:\n\n')
print('  Pesos: %s\n' % ppn_and.weights) # Despliega los pesos del perceptrón
print('  B_andias: %s\n' % ppn_and.bias) # Despliega el b_andias del perceptrón

print("Función IMPLICA")
print('Parámetros del modelo:\n\n')
print('  Pesos: %s\n' % ppn_imp.weights) # Despliega los pesos del perceptrón
print('  B_andias: %s\n' % ppn_imp.bias) # Despliega el b_andias del perceptrón

# Evaluación de los modelos

train_acc_and = ppn_and.evaluate(x_train, y_train_and)
print('Precisión del entrenamiento para AND: %.2f%%' % (train_acc_and*100)) # Despliega la precisión del entrenamiento

sim_and = ppn_and.forward([0.99, 1.001])
print('Salida particular del perceptrón función AND: ', sim_and)

train_acc_imp = ppn_imp.evaluate(x_train, y_train_imp)
print('Precisión del entrenamiento para IMPLICA: %.2f%%' % (train_acc_imp*100)) # Despliega la precisión del entrenamiento

sim_imp = ppn_imp.forward([0.99, 1.001])
print('Salida particular del perceptrón función IMPLICA: ', sim_imp)



# Gráfico 2D de recta de decisión

# Función AND
w_and, b_and = ppn_and.weights, ppn_and.bias

x0_min_and = -0.5
x1_min_and = ( (-(w_and[0] * x0_min_and) - b_and[0]) 
          / w_and[1] )

x0_max_and = 1.5
x1_max_and = ( (-(w_and[0] * x0_max_and) - b_and[0]) 
          / w_and[1] )

plt.figure()
plt.plot([x0_min_and, x0_max_and], [x1_min_and, x1_max_and])
plt.scatter(x_train[y_train_and==0, 0], x_train[y_train_and==0, 1], label='class 0', marker='o')
plt.scatter(x_train[y_train_and==1, 0], x_train[y_train_and==1, 1], label='class 1', marker='s')
plt.title("Función AND")
plt.grid(True)
plt.legend(loc='upper right')



# Función IMPLICA
w_imp, b_imp = ppn_imp.weights, ppn_imp.bias

x0_min_imp = -0.5
x1_min_imp = ( (-(w_imp[0] * x0_min_imp) - b_imp[0]) 
          / w_imp[1] )

x0_max_imp = 1.5
x1_max_imp = ( (-(w_imp[0] * x0_max_imp) - b_imp[0]) 
          / w_imp[1] )

plt.figure()
plt.plot([x0_min_imp, x0_max_imp], [x1_min_imp, x1_max_imp])
plt.scatter(x_train[y_train_imp==0, 0], x_train[y_train_imp==0, 1], label='class 0', marker='o')
plt.scatter(x_train[y_train_imp==1, 0], x_train[y_train_imp==1, 1], label='class 1', marker='s')
plt.title("Función IMPLICA")
plt.grid(True)
plt.legend(loc='upper right')

plt.show()