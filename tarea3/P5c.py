import numpy as np
import cv2 as cv
import os

#Borra terminal en windows
os.system('cls')

# Lee imagen
A = cv.imread("objetos.jpg")

# Transforma a escala de grises
Agris = cv.cvtColor(A, cv.COLOR_BGR2GRAY)

# Especifica valor de umbral
umbral = 240
ret, Abin = cv.threshold(Agris,umbral,1,0)

#Calcula dimensión de matriz de imagen
s = Abin.shape[:]

#Matríz de detección de bordes por laplaciano
Laplaciano3 = np.zeros((s[0], s[1]))

#Se aplica el operador sobre la imagen
for fil in range (1,s[0] - 1):
    for col in range (1,s[1] - 1):
        #Convolución
        Laplaciano3[fil,col] = -1*Abin[fil-1,col-1]-1*Abin[fil-1,col]-1*Abin[fil-1,col+1] \
                              -1*Abin[fil,col-1]+8*Abin[fil,col]-1*Abin[fil,col+1] \
                              -1*Abin[fil+1,col-1]-1*Abin[fil+1,col]-1*Abin[fil+1,col+1]

#Matríz laplaciana 3
Matriz3 = np.array([[-1, -1, -1], [-1, 8,-1],[-1, -1, -1]])

#Visualizar la máscara que se utilizó
print("Tercera máscara laplaciana: \n", Matriz3)

# Visualiza imágenes
cv.imshow("Imagen Bordes Laplaciano3", Laplaciano3)
cv.imshow("Imagen Binaria", 255*Abin)
cv.waitKey(0)