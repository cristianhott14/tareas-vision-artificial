import numpy as np
import cv2 as cv
import os

#Borra terminal en windows
os.system('cls')

# Lee imagen
A = cv.imread("objetos.jpg")

# Transforma a escala de grises
Agris = cv.cvtColor(A, cv.COLOR_BGR2GRAY)

# Especifica valor de umbral
umbral = 241
ret1, Abin = cv.threshold(Agris,umbral,1,0)

#Calcula dimensión de matriz de imagen
s = Abin.shape[:]

Magnitud = np.zeros((s[0], s[1]))
Direccion = np.empty((s[0], s[1]), 'U3')

#Crea vector con resultado de convolución con máscaras Kirsch
R = np.zeros((8), int)

#Se aplica el operador sobre la imagen
for fil in range (1,s[0] - 1):
    for col in range (1,s[1] - 1):
    #Cálculo de r0
        R[0] = -1*Abin[fil-1,col-1]+0*Abin[fil-1,col]+1*Abin[fil-1,col+1] \
               -2*Abin[fil,col-1]+0*Abin[fil,col]+2*Abin[fil,col+1] \
               -1*Abin[fil+1,col-1]+0*Abin[fil+1,col]+1*Abin[fil+1,col+1]
    #Cálculo de r1
        R[1] = 0*Abin[fil-1,col-1]+1*Abin[fil-1,col]+2*Abin[fil-1,col+1] \
               -1*Abin[fil,col-1]+0*Abin[fil,col]+1*Abin[fil,col+1] \
               -2*Abin[fil+1,col-1]-1*Abin[fil+1,col]+0*Abin[fil+1,col+1]
    #Cálculo de r2
        R[2] = 1*Abin[fil-1,col-1]+2*Abin[fil-1,col]+1*Abin[fil-1,col+1] \
              +0*Abin[fil,col-1]+0*Abin[fil,col]+0*Abin[fil,col+1] \
              -1*Abin[fil+1,col-1]-2*Abin[fil+1,col]-1*Abin[fil+1,col+1]
    #Cálculo de r3
        R[3] = 2*Abin[fil-1,col-1]+1*Abin[fil-1,col]+0*Abin[fil-1,col+1] \
              +1*Abin[fil,col-1]+0*Abin[fil,col]-1*Abin[fil,col+1] \
              +0*Abin[fil+1,col-1]-1*Abin[fil+1,col]-2*Abin[fil+1,col+1]
    #Cálculo de r4
        R[4] = -R[0]
    #Cálculo de r5
        R[5] = -R[1]
    #Cálculo de r6
        R[6] = -R[2]
    #Cálculo de r7
        R[7] = -R[3]

    #Cálculo de Magnitud        
        Magnitud[fil,col] = max(R)
        I = np.argmax(R)
        
        #Cálculo de Dirección, si sólo se necesita la Magnitud, se pueden borrar
        #estas líneas para aumentar la velocidad del algoritmo
        #******************************************
        if I == 0: Direccion[fil,col] = "N"
        if I == 1: Direccion[fil,col] = "NO"
        if I == 2: Direccion[fil,col] = "O"
        if I == 3: Direccion[fil,col] = "SO"
        if I == 4: Direccion[fil,col] = "S"
        if I == 5: Direccion[fil,col] = "SE"
        if I == 6: Direccion[fil,col] = "E"
        if I == 7: Direccion[fil,col] = "NE"
        #*******************************************

# Visualiza imágenes
cv.imshow("Imagen Bordes Robinson", Magnitud)
cv.imshow("Imagen Binaria", 255*Abin)
print(Direccion)
cv.waitKey(0)