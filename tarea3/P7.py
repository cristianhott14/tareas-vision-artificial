import numpy as np
import cv2 as cv
import os

#Borra terminal en windows
os.system('cls')

# Lee imagen
img = cv.imread("star.jpg")

# Transforma a escala de grises
imgGris = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

# Especifica valor de eumbral
umbral = 127

# Transforma a imagen binaria
ret, imgBin = cv.threshold(imgGris,umbral,1,0)

#Calcula dimensión de matriz de imagen
s = imgBin.shape[:]

#variables auxiliares
area=0
x1=0
y1=0

#Se aplica el operador sobre la imagen
for fil in range (1,s[0] - 1):
    for col in range (1,s[1] - 1):
        #El area será la suma de pixeles de valor 1, ya que es una imagen binaria
        area=area+imgBin[fil,col]
        
        #Valores auxiliares para el calculo del centroide
        x1= x1+col*imgBin[fil,col]
        y1= y1+fil*imgBin[fil,col]

#Calculo de centroide viston en clases
x=x1/area
y=y1/area

#Se muestra el valor del centroide con el método visto en clases
print("Centroide visto en clases x:",int(x),"y:",int(y))

# Encuentra puntos de contorno
contours,hierarchy = cv.findContours(imgBin, 1, 2)
cnt = contours[0]

# Calcula centroide
M = cv.moments(cnt)
cx = int(M['m10']/M['m00'])
cy = int(M['m01']/M['m00'])

#Se muestra el valor del centroide con la función moments
print("Centroide con fun. moments","x:",cx,"y:",cy)

# Visualiza imagen binaria
cv.imshow("Imagen Binaria", 255*imgBin)
cv.waitKey(0)