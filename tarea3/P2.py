import cv2 as cv
import numpy as np
import os
import math

#Borra terminal en windows
os.system('cls')

#Se lee la imagen
org = cv.imread("objetos.jpg")
img_gris= cv.cvtColor(org,cv.COLOR_BGR2GRAY)

#Dimensiones
num_fil = img_gris.shape[0]
num_col = img_gris.shape[1]  

#Matriz que contendra los bordes
Magnitud = np.zeros((num_fil, num_col))

#Umbral y función para hacer la imagen binaria
umbral= 238
ret, img_bin = cv.threshold(img_gris,umbral,1,0)

#Recorrido de la imagen
for fil in range(1, num_fil - 1):
    for col in range(1, num_col - 1):
        #Diagonal izquierdo al pixel de interes
        D_izq=img_bin[fil,col]-img_bin[fil-1,col-1]
        #Diagonal derecho al pixel de interes
        D_der=img_bin[fil,col]-img_bin[fil-1,col+1]

        #Magnitud del oper de Roberts
        Magnitud[fil,col] = np.sqrt((D_izq)**2+(D_der)**2)

#Muestra la imagen y se cierra al presionar una tecla
cv.imshow("Deteccion bordes Roberts", Magnitud)
cv.waitKey(0)
cv.destroyAllWindows()