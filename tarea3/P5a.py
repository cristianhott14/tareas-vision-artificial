import numpy as np
import cv2 as cv
import os

#Borra terminal en windows
os.system('cls')

# Lee imagen
A = cv.imread("objetos.jpg")

# Transforma a escala de grises
Agris = cv.cvtColor(A, cv.COLOR_BGR2GRAY)

# Especifica valor de umbral
umbral = 240
ret, Abin = cv.threshold(Agris,umbral,1,0)

#Calcula dimensión de matriz de imagen
s = Abin.shape[:]

#Matríz de detección de bordes por laplaciano
Laplaciano1 = np.zeros((s[0], s[1]))

#Se aplica el operador sobre la imagen
for fil in range (1,s[0] - 1):
    for col in range (1,s[1] - 1):
        #Convolución
        Laplaciano1[fil,col] = 0*Abin[fil-1,col-1]-1*Abin[fil-1,col]+0*Abin[fil-1,col+1] \
                              -1*Abin[fil,col-1]+4*Abin[fil,col]-1*Abin[fil,col+1] \
                              +0*Abin[fil+1,col-1]-1*Abin[fil+1,col]+0*Abin[fil+1,col+1]

#Matríz laplaciana 1
Matriz1 = np.array([[0, -1, 0], [-1, 4,-1],[0, -1, 0]])

#Visualizar la máscara que se utilizó
print("Primera máscara laplaciana: \n", Matriz1)

# Visualiza imágenes
cv.imshow("Imagen Bordes Laplaciano1", Laplaciano1)
cv.imshow("Imagen Binaria", 255*Abin)
cv.waitKey(0)