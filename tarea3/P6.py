import numpy as np
import cv2 as cv
import os

#Borra terminal en windows
os.system('cls')

# Lee imagen
img = cv.imread("star.jpg")

# Transforma a escala de grises
imgGris = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

# Especifica valor de eumbral
umbral = 240

# Transforma a imagen binaria
ret, imgBin = cv.threshold(imgGris,umbral,1,0)

#Calcula dimensión de matriz de imagen
s = imgBin.shape[:]

#Matríz de detección de bordes por laplaciano primera máscara
Laplaciano1 = np.zeros((s[0], s[1]))
perimetro=0

#Se aplica el operador sobre la imagen
for fil in range (1,s[0] - 1):
    for col in range (1,s[1] - 1):
        #Convolución de la primera máscara laplaciana, ya que se observa que genera un contorno fino
        Laplaciano1[fil,col] = 0*imgBin[fil-1,col-1]-1*imgBin[fil-1,col]+0*imgBin[fil-1,col+1] \
                              -1*imgBin[fil,col-1]+4*imgBin[fil,col]-1*imgBin[fil,col+1] \
                              +0*imgBin[fil+1,col-1]-1*imgBin[fil+1,col]+0*imgBin[fil+1,col+1]
        #Binarizar los valores después de la convolución
        if int(Laplaciano1[fil,col])<=0:
            Laplaciano1[fil,col]=0
        else:
            Laplaciano1[fil,col]=1
        
        #Suma del contorno
        perimetro=perimetro+int(Laplaciano1[fil,col])

# Encuentra puntos de contorno
contours,hierarchy = cv.findContours(imgBin, 1, 2)
cnt = contours[0]

# Calcula perímetro con función arcLength
perimeter = cv.arcLength(cnt,True)

#Mostrar los valores del calculo de perimetro
print(" Perimetro visto en clases", perimetro,"\n Perimetro con fun. arcLength",int(perimeter))

# Visualiza imagen binaria
cv.imshow("Imagen Bordes Laplaciano1", Laplaciano1)
cv.imshow("Imagen Binaria", 255*imgBin)
cv.waitKey(0)