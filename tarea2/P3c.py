import cv2 as cv
import numpy as np

# Lee imagen A
imgA = cv.imread("foto1.jpg")

#Dimensiones de e imagen, dado que son la mismas dimendiones
num_fil = imgA.shape[0]
num_col = imgA.shape[1]

#Factor de multiplicación
factor = 1.7

#Matriz de ceros con las dimensiones de la imagen
imgMult = np.zeros((num_fil,num_col,3), np.uint8)

for fil in range(num_fil):
    for col in range(num_col):
        if round(int(imgA[fil,col,0]) * factor) <= 255:  #Si la multiplicación es menor o igual a 255, se multiplica
            imgMult[fil,col,0] = round(imgA[fil,col,0] * factor)
        else:
            imgMult[fil,col,0] = 255                            #sino se ajusta a 255. Misma acción para los demas canales
        if round(int(imgA[fil,col,1]) * factor) <= 255:  
            imgMult[fil,col,1] = round(imgA[fil,col,1] * factor)
        else: 
            imgMult[fil,col,1] = 255  
        if round(int(imgA[fil,col,2]) * factor) <= 255: 
            imgMult[fil,col,2] = round(imgA[fil,col,2] * factor)
        else: 
            imgMult[fil,col,2] = 255  

#multiplicación de imagen por factor con función "multiply"
imgMultiply = cv.multiply(imgA, (factor, factor, factor, factor))

#Se muestran las imagenes con las dos restas
cv.imshow("Imagen A", imgA)
cv.imshow("Imagen multiplicada", imgMult)
cv.imshow("Imagen multiplicada con CV", imgMultiply)
cv.waitKey(0)
cv.destroyAllWindows()