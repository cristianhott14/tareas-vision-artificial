import cv2
import os

#Se lee la imagen
img=cv2.imread("Foto1.jpg")
fil = img.shape[0]
col = img.shape[1]

#Borra terminal en windows
os.system('cls')

#Se imprime el total de filas y columnas al usuario
print("Rango de Filas:", fil, " y Columnas:", col)


# Pregunta a usuario vértices de la región a recortar
f1 = int(input("Ingrese la fila del vértice superior izquierdo: "))
f1aux=f1 #variable auxiliar

c1 = int(input("Ingrese la columna del vértice superior izquierdo: "))
c1aux=c1 #variable auxiliar

f2 = int(input("Ingrese la fila del vértice inferior derecho: "))
while f2 == f1aux:  #Se le pide al usuario que ingrese un valor distinto de la primera fila, mientras no lo haga seguira solicitandolo 
    f2 = int(input("Ingrese la fila del vértice inferior derecho distinto al primero: ")) 

c2 = int(input("Ingrese la columna del vértice inferior derecho: "))
while c2 == c1aux: #Se le pide al usuario que ingrese un valor distinto de la primera columna, mientras no lo haga seguira solicitandolo 
    c2 = int(input("Ingrese la columna del vértice inferior derecho distinto al primero: "))



# Comprueba que los datos hayan sido ingresados correctamente
#Condiciones para la primera cordenada (f1,c1)
#En caso de que el valor ingresado sea negativo o mayor a las dimensiones de la imagen, se corrige como 0. Esto dado que la primera coordenada debe ser la menor siempre.
#En caso de que los valores superen la dimensión de la imagen, no se pueden corregir lo valores al máximo de la imagen, ya que entraria en conflicto con la segunda coordenada ingresada,
#si esta toma los valores máximos.
if f1 < 0 or f1 > fil: 
    f1 = 0    
if c1 < 0 or c1 > col:
    c1 = 0

#Condiciones para la primera cordenada (f2,c2)
#En caso de que el valor ingresado sea negativo o mayor a las dimensiones de la imagen, se corrige como al máximo correspondiente. Esto dado que la segunda coordenada debe ser la mayor siempre.
if f2 < 0 or f2 > fil: 
    f2 = fil
if c2 < 0 or c2 > col: 
    c2 = col

#La idea de las condiciones anteriores, es que el programa siempre arroje la imagen, aunque sea la imagen original, en vez de tirar un error al usuario.


#Variables auxiliares para hacer las comparaciones
f2aux=f2
c2aux=c2

#Se compara si los valores de las segunda coordenada son menores a las de la primera, en contrario se invierten
#Si la fila 2 es mayor a la 1, se invierten
if f2 < f1:
    f1=f2aux
    f2=f1aux

#Si la columan 2 es mayor a la 1, se invierten
if c2 < c1:
    c1=c2aux
    c2=c1aux

#Observar coordenadas finales
print("Coordenadas: P1(",f1,",",c1,") y P2(",f2,",",c2,")")

#Imagen recortada
img_recortada = img[f1:f2,c1:c2,:]

#Se muestra la imagen recortada
cv2.imshow("P1 Imagen recortada",img_recortada)
cv2.waitKey(0)
cv2.destroyAllWindows()