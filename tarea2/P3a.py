import cv2 as cv
import numpy as np

# Lee imagen A
imgA = cv.imread("foto1.jpg")

# Lee imagen B
imgB = cv.imread("foto2.jpg")

#Dimensiones de e imagen, dado que son la mismas dimendiones
num_fil = imgA.shape[0]
num_col = imgA.shape[1]

#Matriz de ceros con las dimensiones de la imagen
imgSum = np.zeros((num_fil,num_col,3), np.uint8)

for fil in range(num_fil): 
    for col in range(num_col):
        if int(imgA[fil,col,0]) + int(imgB[fil,col,0]) <= 255:  #Si la suma es menor o igual a 255, se suma
             imgSum[fil,col,0] = imgA[fil,col,0] + imgB[fil,col,0]  
        else:
             imgSum[fil,col,0] = 255                            #sino se ajusta a 255. Misma acción para los demas canales
        if int(imgA[fil,col,1]) + int(imgB[fil,col,1]) <= 255: 
            imgSum[fil,col,1] = imgA[fil,col,1] + imgB[fil,col,1]
        else: 
            imgSum[fil,col,1] = 255
        if int(imgA[fil,col,2]) + int(imgB[fil,col,2]) <= 255:
             imgSum[fil,col,2] = imgA[fil,col,2] + imgB[fil,col,2]
        else: 
            imgSum[fil,col,2] = 255
      
#Suma de imagenes con función de opencv "add"
imgSumCV = cv.add(imgA, imgB)

#Suma ponderada con función de opencv "addWeighte"
imgSumCVweighted = cv.addWeighted(imgA, 0.6, imgB, 0.4, 0.0)

#Se muestran las imagenes con diferentes sumas
cv.imshow("Imagen A", imgA)
cv.imshow("Imagen B", imgB)
cv.imshow("Suma", imgSum)
cv.imshow("Suma CV", imgSumCV)
cv.imshow("Suma Ponderada CV", imgSumCVweighted)
cv.waitKey(0)
cv.destroyAllWindows()