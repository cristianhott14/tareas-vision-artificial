import cv2 as cv
import numpy as np
import os

#Borra terminal en windows
os.system('cls')

# Pregunta a usuario por el factor del zoom
factor = int(input("Ingrese el factor de zoom entre 1 y 10: "))
while factor <= 1 or factor >= 10:  #Se le pide al usuario que ingrese un valor entre 1 a 10, mientras no lo haga seguira solicitandolo 
    factor = int(input("Por favor, ingrese el factor de zoom entre 1 y 10: ")) 

# Create point matrix get coordinates of mouse click on image
point_matrix = np.zeros((2,2),int)
 
counter = 0
flag = 0
def mousePoints(event,x,y,flags,params):
    global counter
    # Left button mouse click event opencv
    if event == cv.EVENT_LBUTTONDOWN:
        point_matrix[counter] = x,y
        counter = counter + 1
 
#Se lee la imagen
img = cv.imread("Foto1.jpg")
 
while True:
    #for x in range (0,2):
    #  cv.circle(img,(point_matrix[x][0],point_matrix[x][1]),3,(0,255,0),cv.FILLED)     #Al realizar mucho zoom el circulo molesta en la imagen
 
    if counter == 2:
        starting_x = point_matrix[0][0]
        starting_y = point_matrix[0][1]
 
        ending_x = point_matrix[1][0]
        ending_y = point_matrix[1][1]
        # Draw rectangle for area of interest
        cv.rectangle(img, (starting_x-1, starting_y-1), (ending_x+1, ending_y+1), (0, 255, 0), 1)
 
        # Cropping image
        img_cropped = img[starting_y:ending_y, starting_x:ending_x]
        cv.imshow("ROI", img_cropped)
 
    # Showing original image
    cv.imshow("Original Image ", img)
    cv.setWindowProperty("Original Image ", cv.WND_PROP_TOPMOST, 1)
    # Mouse click event on original image
    cv.setMouseCallback("Original Image ", mousePoints)

    # Refreshing window all time
    cv.waitKey(0)

    if flag == 1: break
    if counter == 2: flag += 1

# Zoom visto en clases
img_pre_zoom  = img_cropped.copy()

for x in range(factor - 1):
    #Dimensiones de imagen pre zoom
    num_fil = img_pre_zoom.shape[0]
    num_col = img_pre_zoom.shape[1]

    #matriz de la imagen extendida 
    img_extendida = np.zeros((2*num_fil, 2*num_col,3), np.uint8)

    for fil in range(num_fil):
        for col in range(num_col):
             img_extendida[2*fil,2*col] = img_pre_zoom[fil,col] # se rellena la matriz con valores de la imagen pre zoom y haciendo saltos de filas y columnas de ceros

    #Imagen con zoom visto en clases
    img_zoom  = img_extendida.copy()

    #Convolución
    for fil in range(1, 2*num_fil - 1):
        for col in range(1, 2*num_col - 1):
            img_zoom[fil,col] = (img_extendida[fil-1,col-1]*0.25)+(img_extendida[fil-1,col]*0.5)+(img_extendida[fil-1,col+1]*0.25)+(img_extendida[fil,col-1]*0.5)+(img_extendida[fil,col]*1)+(img_extendida[fil,col+1]*0.5)+(img_extendida[fil+1,col-1]*0.25)+(img_extendida[fil+1,col]*0.5)+(img_extendida[fil+1,col+1]*0.25) #se multiplica por los valores de la máscara de convolución

    #Hacemos una copia de la imagen aumentada para volver a realizar el procedimiento sobre el zoom ya hecho
    img_pre_zoom = img_zoom.copy()

# zoom de cv
def zoom(img, zoom_factor): 
    return cv.resize(img, None, fx=zoom_factor, fy=zoom_factor)
#Se realiza zoom con la función de open cv
zoomed_and_cropped = zoom(img_cropped, factor)

#Se presentan las imagenes con zoom
cv.imshow("Imagen zoom convolución", img_zoom)
cv.imshow("Imagen zoom cv", zoomed_and_cropped )
cv.waitKey(0)
cv.destroyAllWindows()