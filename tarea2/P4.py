import cv2 as cv
import sys
import numpy as np

#Función para generar ruido
def noisy(noise_typ,image):
   if noise_typ == "gauss":
      row,col,ch= image.shape
      mean = 0
      var = 0.1
      sigma = var**0.5
      gauss = np.random.normal(mean,sigma,(row,col,ch))
      gauss = gauss.reshape(row,col,ch)
      noisy = image + gauss
      return noisy
   elif noise_typ == "s&p":
      row,col,ch = image.shape
      s_vs_p = 0.5
      amount = 0.004
      out = np.copy(image)
      # Salt mode
      num_salt = np.ceil(amount * image.size * s_vs_p)
      coords = [np.random.randint(0, i - 1, int(num_salt))
              for i in image.shape]
      out[tuple(coords)] = 1

      # Pepper mode
      num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
      coords = [np.random.randint(0, i - 1, int(num_pepper))
              for i in image.shape]
      out[tuple(coords)] = 0
      return out
   elif noise_typ == "poisson":
      vals = len(np.unique(image))
      vals = 2 ** np.ceil(np.log2(vals))
      noisy = np.random.poisson(image * vals) / float(vals)
      return noisy
   elif noise_typ =="speckle":
      row,col,ch = image.shape
      gauss = np.random.randn(row,col,ch)
      gauss = gauss.reshape(row,col,ch)        
      noisy = image + image * gauss
      return noisy

#Lee la imagen
img = cv.imread("foto1.jpg")

if img is None:
    sys.exit("Could not read the image.")

#Dimensiones de la imagen
num_fil = img.shape[0]
num_col = img.shape[1]

#Se crea una imagen con ruido y otras a las que se le aplicará el filtro
imgRuido = noisy('s&p',img)
imgFiltroPro  = imgRuido.copy()
imgFiltroMed  = imgRuido.copy()
imgFiltroCV = imgRuido.copy()

for fil in range(1, num_fil - 1):
    for col in range(1, num_col - 1):
        #Operación de filtro Promedio
        imgFiltroPro[fil,col,0]=round((int(imgRuido[fil-1,col-1,0])+int(imgRuido[fil-1,col,0])+int(imgRuido[fil-1,col+1,0])+int(imgRuido[fil,col-1,0])+int(imgRuido[fil,col,0])+int(imgRuido[fil,col+1,0])+int(imgRuido[fil+1,col-1,0])+int(imgRuido[fil+1,col,0])+int(imgRuido[fil+1,col+1,0]))/9)
        imgFiltroPro[fil,col,1]=round((int(imgRuido[fil-1,col-1,1])+int(imgRuido[fil-1,col,1])+int(imgRuido[fil-1,col+1,1])+int(imgRuido[fil,col-1,1])+int(imgRuido[fil,col,1])+int(imgRuido[fil,col+1,1])+int(imgRuido[fil+1,col-1,1])+int(imgRuido[fil+1,col,1])+int(imgRuido[fil+1,col+1,1]))/9)
        imgFiltroPro[fil,col,2]=round((int(imgRuido[fil-1,col-1,2])+int(imgRuido[fil-1,col,2])+int(imgRuido[fil-1,col+1,2])+int(imgRuido[fil,col-1,2])+int(imgRuido[fil,col,2])+int(imgRuido[fil,col+1,2])+int(imgRuido[fil+1,col-1,2])+int(imgRuido[fil+1,col,2])+int(imgRuido[fil+1,col+1,2]))/9)        

        #Operación de filtro Medio
        #Se crea  una lista con 9 valores (en el canal correspondiente)
        ListaA=[int(imgRuido[fil-1,col-1,0]),int(imgRuido[fil-1,col,0]),int(imgRuido[fil-1,col+1,0]),int(imgRuido[fil,col-1,0]),int(imgRuido[fil,col,0]),int(imgRuido[fil,col+1,0]),int(imgRuido[fil+1,col-1,0]),int(imgRuido[fil+1,col,0]),int(imgRuido[fil+1,col+1,0])]
        ListaB=[int(imgRuido[fil-1,col-1,1]),int(imgRuido[fil-1,col,1]),int(imgRuido[fil-1,col+1,1]),int(imgRuido[fil,col-1,1]),int(imgRuido[fil,col,1]),int(imgRuido[fil,col+1,1]),int(imgRuido[fil+1,col-1,1]),int(imgRuido[fil+1,col,1]),int(imgRuido[fil+1,col+1,1])]
        ListaC=[int(imgRuido[fil-1,col-1,2]),int(imgRuido[fil-1,col,2]),int(imgRuido[fil-1,col+1,2]),int(imgRuido[fil,col-1,2]),int(imgRuido[fil,col,2]),int(imgRuido[fil,col+1,2]),int(imgRuido[fil+1,col-1,2]),int(imgRuido[fil+1,col,2]),int(imgRuido[fil+1,col+1,2])]
        
        #Se ordena los valores de menor a mayor
        ordenA=sorted(ListaA)
        ordenB=sorted(ListaB)
        ordenC=sorted(ListaC)

        #Se encuentra la posición media
        posmedA=round(len(ordenA)/2)
        posmedB=round(len(ordenB)/2)
        posmedC=round(len(ordenC)/2)

        #Se asigna el valor de la posición media a la imagen
        imgFiltroMed[fil,col,0]=ordenA[posmedA]
        imgFiltroMed[fil,col,1]=ordenB[posmedB]
        imgFiltroMed[fil,col,2]=ordenC[posmedC]


#Filtro medio con la función "medianBlur" de opencv
mgFiltroCV = cv.medianBlur(imgFiltroCV, 3) 

#Se presentan las imagenes con distintos filtros
cv.imshow("Imagen ruido", imgRuido)
cv.imshow("Imagen filtro promedio", imgFiltroPro)
cv.imshow("Imagen filtro medio", imgFiltroMed)
cv.imshow("Imagen filtrada Filtro Medio OpenCV", imgFiltroCV)
cv.waitKey(0)