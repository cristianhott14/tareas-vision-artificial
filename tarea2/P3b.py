import cv2 as cv
import numpy as np

# Lee imagen A
imgA = cv.imread("foto1.jpg")

# Lee imagen B
imgB = cv.imread("foto2.jpg")

#Dimensiones de e imagen, dado que son la mismas dimendiones
num_fil = imgA.shape[0]
num_col = imgA.shape[1]

#Matriz de ceros con las dimensiones de la imagen
imgRest = np.zeros((num_fil,num_col,3), np.uint8)

for fil in range(num_fil):
    for col in range(num_col):
        if int(imgA[fil,col,0]) - int(imgB[fil,col,0]) >= 0:  #Si la resta es mayor a 0, se restan
             imgRest[fil,col,0] = imgA[fil,col,0] + imgB[fil,col,0]  
        else:
             imgRest[fil,col,0] = 0                            #sino se ajusta a 0. Misma acción para los demás canales
        if int(imgA[fil,col,1]) - int(imgB[fil,col,1]) >= 0: 
            imgRest[fil,col,1] = imgA[fil,col,1] + imgB[fil,col,1]
        else: 
            imgRest[fil,col,1] = 0
        if int(imgA[fil,col,2]) - int(imgB[fil,col,2]) >= 0:
             imgRest[fil,col,2] = imgA[fil,col,2] + imgB[fil,col,2]
        else: 
            imgRest[fil,col,2] = 0

#Resta de imagenes con función "subtract"
imgRestSubtract = cv.subtract(imgA, imgB)

#Se muestran las imagenes con las dos restas
cv.imshow("Imagen A", imgA)
cv.imshow("Imagen B", imgB)
cv.imshow("Resta", imgRest)
cv.imshow("Resta CV", imgRestSubtract)
cv.waitKey(0)
cv.destroyAllWindows()