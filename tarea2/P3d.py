import cv2 as cv
import numpy as np

# Lee imagen A
imgA = cv.imread("foto1.jpg")

#Dimensiones de e imagen, dado que son la mismas dimendiones
num_fil = imgA.shape[0]
num_col = imgA.shape[1]

#Factor de multiplicación
factor = 1.7

#Matriz de ceros con las dimensiones de la imagen
imgDivd = np.zeros((num_fil,num_col,3), np.uint8)

for fil in range(num_fil):
    for col in range(num_col): 
            imgDivd[fil,col,0] = round(imgA[fil,col,0] / factor)
            imgDivd[fil,col,1] = round(imgA[fil,col,1] / factor)
            imgDivd[fil,col,2] = round(imgA[fil,col,2] / factor)


#División de imagen por factor con función "Divide"
imgDivide = cv.divide(imgA, (factor, factor, factor, factor))

#Se muestran las imagenes con las dos restas
cv.imshow("Imagen A", imgA)
cv.imshow("Imagen multiplicada", imgDivd)
cv.imshow("Imagen multiplicada con CV", imgDivide)
cv.waitKey(0)
cv.destroyAllWindows()