import cv2 as cv
import numpy as np

#Umbral para binarizar las imagenes
umbral= 250

#Unos
#Se forma el numero 1 y luego se utiliza una copia para las modificaciones
img_1= np.zeros((10,10))
img_1[2:8,5]=255;img_1[8,4:7]=255;img_1[3,4]=255;img_1[4,3]=255 

#5 modificaciones del 1
img_1_1=img_1.copy()
img_1_1[8,8:10]=255;img_1_1[5,5]=0;
ret, img_bin_1_1 = cv.threshold(img_1_1,umbral,1,0)

img_1_2=img_1.copy()
img_1_2[3:7,2]=255;img_1_2[2,8]=255;
ret, img_bin_1_2 = cv.threshold(img_1_2,umbral,1,0)

img_1_3=img_1.copy()
img_1_3[3:6,8]=255;img_1_3[9,3]=255;
ret, img_bin_1_3 = cv.threshold(img_1_3,umbral,1,0)

img_1_4=img_1.copy()
img_1_4[5,4:8]=255;img_1_4[9,0]=255;
ret, img_bin_1_4 = cv.threshold(img_1_4,umbral,1,0)

img_1_4=img_1.copy()
img_1_4[0,0]=255;img_1_4[9,2:6]=255;
ret, img_bin_1_4 = cv.threshold(img_1_4,umbral,1,0)

img_1_5=img_1.copy()
img_1_5[0,5]=255;img_1_5[9,2]=255;img_1_5[4,4]=255;img_1_5[2,9]=255;
ret, img_bin_1_5 = cv.threshold(img_1_5,umbral,1,0)

#Se muestran las 5 matrices del 1 modificado
print("Numero 1_1:\n")
print((1-img_bin_1_1))
print("Numero 1_2:\n")
print((1-img_bin_1_2))
print("Numero 1_3:\n")
print((1-img_bin_1_3))
print("Numero 1_4:\n")
print((1-img_bin_1_4))
print("Numero 1_5:\n")
print((1-img_bin_1_5))

#Dos
#Se forma el numero 2 y luego se utiliza una copia para las modificaciones
img_2= np.zeros((10,10))
img_2[2,3]=255;img_2[1,4:6]=255;img_2[2:4,6]=255;img_2[4,5]=255;img_2[5,4]=255;img_2[6,3]=255;img_2[7,3:7]=255;

#5 modificaciones del 2
img_2_1=img_2.copy()
img_2_1[2,2]=255;img_2_1[2,2]=255;img_2_1[9,3:6]=255
ret, img_bin_2_1 = cv.threshold(img_2_1,umbral,1,0)

img_2_2=img_2.copy()
img_2_2[2,6:9]=255;
ret, img_bin_2_2 = cv.threshold(img_2_2,umbral,1,0)

img_2_3=img_2.copy()
img_2_3[4:8,6]=255;
ret, img_bin_2_3 = cv.threshold(img_2_3,umbral,1,0)

img_2_4=img_2.copy()
img_2_4[0,0]=255;img_2_4[1,5]=0;img_2_4[7,6]=0;img_2_4[7,7]=255;
ret, img_bin_2_4 = cv.threshold(img_2_4,umbral,1,0)

img_2_5=img_2.copy()
img_2_5[8,8]=255;img_2_5[7,5:9]=0;img_2_5[3,4]=255;
ret, img_bin_2_5 = cv.threshold(img_2_5,umbral,1,0)

#Se muestran las 5 matrices del 2 modificado
print("Numero 2_1:\n")
print((1-img_bin_2_1))
print("Numero 2_2:\n")
print((1-img_bin_2_2))
print("Numero 2_3:\n")
print((1-img_bin_2_3))
print("Numero 2_4:\n")
print((1-img_bin_2_4))
print("Numero 2_5:\n")
print((1-img_bin_2_5))

#Tres
#Se forma el numero 3 y luego se utiliza una copia para las modificaciones
img_3= np.zeros((10,10))
img_3[3,2]=255;img_3[2,3:6]=255;img_3[3:5,6]=255;img_3[5,4:6]=255;img_3[6:8,6]=255;img_3[8,3:6]=255;img_3[7,2]=255;

#5 modificaciones del 3
img_3_1=img_3.copy()
img_3_1[2,2]=255; img_3_1[2:6,9]=255;
ret, img_bin_3_1 = cv.threshold(img_3_1,umbral,1,0)

img_3_2=img_3.copy()
img_3_2[2,2]=255; img_3_2[9,9]=255;img_3_2[5,3]=255;img_3_2[5,0]=255;img_3_2[5,9]=255;
ret, img_bin_3_2 = cv.threshold(img_3_2,umbral,1,0)

img_3_3=img_3.copy()
img_3_3[2:7,4]=255;
ret, img_bin_3_3 = cv.threshold(img_3_3,umbral,1,0)

img_3_4=img_3.copy()
img_3_4[5,6]=255;img_3_4[2,6]=255;img_3_4[8,6]=255;
ret, img_bin_3_4 = cv.threshold(img_3_4,umbral,1,0)

img_3_5=img_3.copy()
img_3_5[4,8]=255;img_3_5[3,9]=255;img_3_5[5,7]=255;img_3_5[2,8]=255;
ret, img_bin_3_5 = cv.threshold(img_3_5,umbral,1,0)

#Se muestran las 5 matrices del 3 modificado
print("Numero 3_1:\n")
print((1-img_bin_3_1))
print("Numero 3_2:\n")
print((1-img_bin_3_2))
print("Numero 3_3:\n")
print((1-img_bin_3_3))
print("Numero 3_4:\n")
print((1-img_bin_3_4))
print("Numero 3_5:\n")
print((1-img_bin_3_5))

#Cuatros
#Se forma el numero 4 y luego se utiliza una copia para las modificaciones
img_4= np.zeros((10,10))
img_4[2:5,3]=255; img_4[5,3:6]=255; img_4[2:9,6]=255

#5 modificaciones del 4
img_4_1=img_4.copy()
img_4_1[1,5:9]=255
ret, img_bin_4_1 = cv.threshold(img_4_1,umbral,1,0)

img_4_2 = img_4.copy()
img_4_2[2:5,0]=255
ret, img_bin_4_2 = cv.threshold(img_4_2,umbral,1,0)

img_4_3 = img_4.copy()
img_4_3[5,4:6]=0;img_4_3[5,7]=255
ret, img_bin_4_3 = cv.threshold(img_4_3,umbral,1,0)

img_4_4 = img_4.copy()
img_4_4[8,3:6]=255
ret, img_bin_4_4 = cv.threshold(img_4_4,umbral,1,0)

img_4_5 = img_4.copy()
img_4_5[0,3]=255;img_4_5[9,5]=255;img_4_5[4:6,9]=255
ret, img_bin_4_5 = cv.threshold(img_4_5,umbral,1,0)

#Se muestran las 5 matrices del 4 modificado
print("Numero 4_1:\n")
print((1-img_bin_4_1))
print("Numero 4_2:\n")
print((1-img_bin_4_2))
print("Numero 4_3:\n")
print((1-img_bin_4_3))
print("Numero 4_4:\n")
print((1-img_bin_4_4))
print("Numero 4_5:\n")
print((1-img_bin_4_5))

#Se muestran las imagenes de cada modifcación
cv.imshow('1_1',1-img_bin_1_1)
cv.imshow('1_2',1-img_bin_1_2)
cv.imshow('1_3',1-img_bin_1_3)
cv.imshow('1_4',1-img_bin_1_4)
cv.imshow('1_5',1-img_bin_1_5)

cv.imshow('2_1',1-img_bin_2_1)
cv.imshow('2_2',1-img_bin_2_2)
cv.imshow('2_3',1-img_bin_2_3)
cv.imshow('2_4',1-img_bin_2_4)
cv.imshow('2_5',1-img_bin_2_5)

cv.imshow('3_1',1-img_bin_3_1)
cv.imshow('3_2',1-img_bin_3_2)
cv.imshow('3_3',1-img_bin_3_3)
cv.imshow('3_4',1-img_bin_3_4)
cv.imshow('3_5',1-img_bin_3_5)

cv.imshow('4_1',1-img_bin_4_1)
cv.imshow('4_2',1-img_bin_4_2)
cv.imshow('4_3',1-img_bin_4_3)
cv.imshow('4_4',1-img_bin_4_4)
cv.imshow('4_5',1-img_bin_4_5)
cv.waitKey(0)
cv.destroyAllWindows()