import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

img = cv.imread("foto1.jpg")

num_fil= img.shape[0]
num_col= img.shape[1]

img_gris = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
Y = cv.calcHist([img],[0],None,[256],[0,256])


Y2 = Y*0
img_gris_ecualizada  = img_gris.copy()

#Paso I
for cont in range(256):
    for k in range(cont):
        Y2[cont]=Y2[cont]+Y[k]

#Paso II
suma = num_fil*num_col #Pixeles totales
Y2 = Y2/suma

#Paso III
Y2 = np.round(Y2*255,0)

#Paso IV
for fil in range(num_fil):
    for col in range(num_col):
        img_gris_ecualizada[fil,col] = Y2[img_gris_ecualizada[fil,col]]


cv.imshow("Imagen original", img_gris)
cv.imshow("Imagen ecualizada", img_gris_ecualizada)

fig = plt.figure(num='Comparación de histogramas', figsize=(10, 4))

figA = fig.add_subplot(121)
figA.set_xlabel('Nivel de gris')
figA.set_ylabel('Magnitud')
figA.set_title('Histograma original')

figB = fig.add_subplot(122)
figB.set_xlabel('Nivel de gris')
figB.set_ylabel('Magnitud')
figB.set_title('Histograma ecualizado')

plt.subplot(121), plt.hist(img_gris.ravel(),256,[0,256])
plt.subplot(122), plt.hist(img_gris_ecualizada.ravel(),256,[0,256])
plt.show()