import cv2 as cv
import numpy as np
import os


#Borra terminal en windows
os.system('cls')

# Pregunta a usuario por el factor del zoom
factor = int(input("Ingrese el factor del zoom a aplicar en la imagen:"))
 
# Create point matrix get coordinates of mouse click on image
point_matrix = np.zeros((2,2),int)
 
counter = 0
flag = 0
def mousePoints(event,x,y,flags,params):
    global counter
    # Left button mouse click event opencv
    if event == cv.EVENT_LBUTTONDOWN:
        point_matrix[counter] = x,y
        counter = counter + 1
 
# Read image
img = cv.imread("foto1.jpg")
 
while True:
    for x in range (0,2):
        cv.circle(img,(point_matrix[x][0],point_matrix[x][1]),1,(0,255,0),cv.FILLED)
 
    if counter == 2:
        starting_x = point_matrix[0][0]
        starting_y = point_matrix[0][1]
 
        ending_x = point_matrix[1][0]
        ending_y = point_matrix[1][1]
        # Draw rectangle for area of interest
        cv.rectangle(img, (starting_x-1, starting_y-1), (ending_x+1, ending_y+1), (0, 255, 0), 1)
 
        # Cropping image
        img_cropped = img[starting_y:ending_y, starting_x:ending_x]
        cv.imshow("ROI", img_cropped)
 
    # Showing original image
    cv.imshow("Original Image ", img)
    cv.setWindowProperty("Original Image ", cv.WND_PROP_TOPMOST, 1)
    # Mouse click event on original image
    cv.setMouseCallback("Original Image ", mousePoints)

    # Refreshing window all time
    cv.waitKey(0) 

    if flag == 1: break
    if counter == 2: flag += 1

# Zoom visto en clases
img_pre_zoom  = img_cropped.copy()

for x in range(factor - 1):
    #Obtenemos las filas y columnas
    num_fil = img_pre_zoom.shape[0]
    num_col = img_pre_zoom.shape[1]

    #Mostramos en consola el tamaño de la imagen por cada zoom
    print('Tamaño de imagen con zoom '+ str(x+1)+'x : '+str(num_fil)+' x '+str(num_col)+' = '+str(num_fil*num_col))

    img_extendida = np.zeros((2*num_fil, 2*num_col,3), np.uint8)

    for fil in range(num_fil):
        for col in range(num_col):
             img_extendida[2*fil,2*col] = img_pre_zoom[fil,col]


    img_zoom  = img_extendida.copy()

    #Aplicamos la máscara de convolución
    for fil in range(1, 2*num_fil - 1):
        for col in range(1, 2*num_col - 1):
            img_zoom[fil,col] = (img_extendida[fil-1,col-1]*0.25)+(img_extendida[fil-1,col]*0.5)+(img_extendida[fil-1,col+1]*0.25)+(img_extendida[fil,col-1]*0.5)+(img_extendida[fil,col]*1)+(img_extendida[fil,col+1]*0.5)+(img_extendida[fil+1,col-1]*0.25)+(img_extendida[fil+1,col]*0.5)+(img_extendida[fil+1,col+1]*0.25)
    
    #Hacemos una copia de la imagen aumentada para volver a realizar el procedimiento sobre el zoom ya hecho
    img_pre_zoom = img_zoom.copy()

# zoom de cv
def zoom(img, zoom_factor):
    return cv.resize(img, None, fx=zoom_factor, fy=zoom_factor)

zoomed_and_cropped = zoom(img_cropped, factor)

#Imprimimos la resolución de la imagen aumentada mediante el comando resize de opencv
imgR_fil = zoomed_and_cropped.shape[0]
imgR_col = zoomed_and_cropped.shape[1]
print('Tamaño de la imgen con resize y factor',str(factor),' : ',imgR_fil,'x',imgR_col,'=',imgR_fil*imgR_col)


#Mostramos las imagenes aumentadas mediante los dos métodos
cv.imshow("Imagen zoom metodo de convolución", img_zoom)
cv.imshow("Imagen zoom cv", zoomed_and_cropped )
cv.waitKey(0)