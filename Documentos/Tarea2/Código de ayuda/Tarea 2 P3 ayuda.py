import cv2 as cv
import numpy as np

# Read image A
imgA = cv.imread(r'C:\Users\gschl\Documents\Uach\I Semestre 2022\ELEP233\Tareas\Archivos Python\Tarea 2\ImageA.jpg')

# Read image B
imgB = cv.imread(r'C:\Users\gschl\Documents\Uach\I Semestre 2022\ELEP233\Tareas\Archivos Python\Tarea 2\ImageB.jpg')

num_fil = imgA.shape[0]
num_col = imgA.shape[1]

imgSum = np.zeros((num_fil,num_col,3), np.uint8)

for fil in range(num_fil):
    for col in range(num_col):
        if int(imgA[fil,col,0]) + int(imgB[fil,col,0]) <= 255: imgSum[fil,col,0] = imgA[fil,col,0] + imgB[fil,col,0]
        else: imgSum[fil,col,0] = 255
        if int(imgA[fil,col,1]) + int(imgB[fil,col,1]) <= 255: imgSum[fil,col,1] = imgA[fil,col,1] + imgB[fil,col,1]
        else: imgSum[fil,col,1] = 255
        if int(imgA[fil,col,2]) + int(imgB[fil,col,2]) <= 255: imgSum[fil,col,2] = imgA[fil,col,2] + imgB[fil,col,2]
        else: imgSum[fil,col,2] = 255
      

imgSumCV = cv.add(imgA, imgB)
imgSumCVweighted = cv.addWeighted(imgA, 0.6, imgB, 0.4, 0.0)
cv.imshow("Imagen A", imgA)
cv.imshow("Imagen B", imgB)
cv.imshow("Suma", imgSum)
cv.imshow("Suma CV", imgSumCV)
cv.imshow("Suma Ponderada CV", imgSumCVweighted)
imgRest = cv.subtract(imgA, imgB)
cv.imshow("Resta CV", imgRest)

factor = 1.2
imgMultiply = cv.multiply(imgA, (factor, factor, factor, factor))

cv.imshow('imgMultiply', imgMultiply)


cv.waitKey(0)