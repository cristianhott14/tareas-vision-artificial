import cv2
import os

# Read image
# img = cv.imread(r'C:\Users\gschl\Documents\Uach\I Semestre 2022\ELEP233\Tareas\Archivos Python\Tarea 1\papagayo.jpg')
img=cv2.imread("Foto1.jpg")
fil = img.shape[0]
col = img.shape[1]

#Borra terminal en windows
os.system('cls')

#Se imprime el total de filas y columnas al usuario
print("Filas:", fil, " y Columnas:", col)

# Pregunta a usuario vértices de la región a recortar
f1 = int(input("Ingrese la fila del vértice superior izquierdo: "))
c1 = int(input("Ingrese la columna del vértice superior izquierdo: "))
f2 = int(input("Ingrese la fila del vértice inferior derecho: "))
c2 = int(input("Ingrese la columna del vértice inferior derecho: "))

# Comprueba que los datos hayan sido ingresados correctamente
#En caso de que el valor ingresado sea negativo, se corrige como 0
#En caso de que el valor ingresado sea mayor a las dimensiones de la imagen, se ingresa el valor maximo correspondiente

if f1 < 0: f1 = 0          
if f1 > fil: f1 = fil       

if f2 < 0: f2 = 0
if f2 > fil: f2 = fil

if c1 < 0: c1 = 0
if c1 > col: c1 = col

if c2 < 0: c2 = 0
if c2 > col: c2 = col

#Variables auxiliares para hacer las comparaciones
f1aux=f1
f2aux=f2

c1aux=c1
c2aux=c2

#Se compara si los valores de las segunda coordenada son menores a las de la primera, en contrario se invierten
#Si la fila 2 es mayor a la 1, se invierten
if f2 < f1:
    f1=f2aux
    f2=f1aux

#Si la columan 2 es mayor a la 1, se invierten
if c2 < c1:
    c1=c2aux
    c2=c1aux




#Observar coordenadas
#print(f1,c1,f2,c2)

#Imagen recortada
img_recortada = img[f1:f2,c1:c2,:]

#Se muestra la imagen recortada
cv2.imshow("a",img_recortada)
cv2.waitKey(0)
cv2.destroyAllWindows()