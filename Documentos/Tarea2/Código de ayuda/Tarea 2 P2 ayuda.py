import cv2 as cv
import numpy as np
import os


#Borra terminal en windows
os.system('cls')

# Pregunta a usuario por el factor del zoom
factor = int(input("Ingrese el factor del zoom a aplicar en la imagen:"))
 
# Create point matrix get coordinates of mouse click on image
point_matrix = np.zeros((2,2),int)
 
counter = 0
flag = 0
def mousePoints(event,x,y,flags,params):
    global counter
    # Left button mouse click event opencv
    if event == cv.EVENT_LBUTTONDOWN:
        point_matrix[counter] = x,y
        counter = counter + 1
 
# Read image
img = cv.imread("Foto1.jpg")
 
while True:
    for x in range (0,2):
        cv.circle(img,(point_matrix[x][0],point_matrix[x][1]),3,(0,255,0),cv.FILLED)
 
    if counter == 2:
        starting_x = point_matrix[0][0]
        starting_y = point_matrix[0][1]
 
        ending_x = point_matrix[1][0]
        ending_y = point_matrix[1][1]
        # Draw rectangle for area of interest
        cv.rectangle(img, (starting_x, starting_y), (ending_x, ending_y), (0, 255, 0), 3)
 
        # Cropping image
        img_cropped = img[starting_y:ending_y, starting_x:ending_x]
        cv.imshow("ROI", img_cropped)
 
    # Showing original image
    cv.imshow("Original Image ", img)
    cv.setWindowProperty("Original Image ", cv.WND_PROP_TOPMOST, 1)
    # Mouse click event on original image
    cv.setMouseCallback("Original Image ", mousePoints)

    # Refreshing window all time
    cv.waitKey(0)

    if flag == 1: break
    if counter == 2: flag += 1

# Zoom visto en clases
img_pre_zoom  = img_cropped.copy()

for x in range(factor - 1):

    num_fil = img_pre_zoom.shape[0]
    num_col = img_pre_zoom.shape[1]




    img_extendida = np.zeros((2*num_fil, 2*num_col,3), np.uint8)

    for fil in range(num_fil):
        for col in range(num_col):
             img_extendida[2*fil,2*col] = img_pre_zoom[fil,col]


    img_zoom  = img_extendida.copy()


    for fil in range(1, 2*num_fil - 1):
        for col in range(1, 2*num_col - 1):
  




# zoom de cv
def zoom(img, zoom_factor): 
    return cv.resize(img, None, fx=zoom_factor, fy=zoom_factor)

zoomed_and_cropped = zoom(img_cropped, factor)








cv.imshow("Imagen zoom cv", zoomed_and_cropped )
cv.waitKey(0)
cv2.destroyAllWindows()

