import cv2 as cv
from img_functions import etiq_recort_resc

# Se realiza un for para ir abriendo las imagenes una en una
# y luego aplicar las funciones de binarizar,etiquetar,recortar y rescalar
# con la función etiq_recort_resc() a cada imagen, las cuales se guardaran en 
# la carpeta "cropped_images". Este proceso se realiza por separado al 
# entrenamiento, ya que tiene un alto costo en tiempo.

for k in range(10):         # barrido por s10 imagenes
    print("Imagen",k+1)         # se imprime en que imagen va
    img=cv.imread('images/'+str(k+1)+'.png')    # lee la imagen y luego ira cambiando
    for i in range(36):             # barrido por cada caracter
        img_rescal=etiq_recort_resc(img,i+1)        #se aplica la función al caracter de la imagen en cuestión
        cv.imwrite('cropped_images2/'+str(k+1)+'_'+str(i+1)+'.png',img_rescal*255)       #Se guarda la imagen como uint8, ya que opencv no guarda imagenes binarizadas
        print("Caracter",i+1)       # imprime en el caracter que va