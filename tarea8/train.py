import cv2 as cv
import numpy as np
import time
from backpropagation import* #Incorpora clase con Backpropagation

ini = time.time()
np.set_printoptions(threshold=np.inf) #Aumentar el rango de visualización de las matrices


def gray_bin_fil(img):
    img_gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY) #Transforma a escala de grises
    ret1, img_bin = cv.threshold(img_gray,250,1,0) #Binariza la imagen
    img_fil = np.array(img_bin.reshape(-1)) #Transforma la matriz de 10 x 10 a 1 x 100
    #print(np.shape(img1_fil))
    return img_fil


# Parámetros de la red
#[R S1 S2 S3] Agregar elementos según número de capas deseado
#Agregar una función de transferencia por cada capa de la red 1:sigmoidal, 2:tanh,  3:lineal.
red = Backprop([100,20,5,3,1], [1,1,2,3])

red.alfa = 0.001 # tasa de aprendizaje de la red
red.num_epochs = 5000 # número máximo de épocas de entrenemiento
red.target_error = 1E-5 # Error objetivo de entrenmiento


# Vector de simulación 
Img_sim=[]
for j in range(36):
    img10=cv.imread('cropped_images/10_'+str(j+1)+'.png')
    Img_sim.append(gray_bin_fil(img10))

Img_simt = np.transpose(Img_sim)
#print(Imgsim10)
#print(np.shape(Imgsim10))


# Vector de entrenamiento P
P=[]
for k in range(9):
    for i in range(36):
        imgs=cv.imread('cropped_images/'+str(k+1)+'_'+str(i+1)+'.png')
        P.append(gray_bin_fil(imgs))
Pt = np.transpose(P)
#print(np.shape(Pt))

#B=P[288]
#AA=B.reshape(10,-1)
#print(AA*288)


# Vector de salida T
T = np.array([[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,
               1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,
               1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,
               1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,
               1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,
               1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,
               1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,
               1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,
               1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36      
               ]])

#print(np.shape(T))

# Inicializa pesos y ganancias
red.W_init(0.001)
red.B_init(0.1)

# Entrena la red neuronal
red.train(Pt, T) 

print("Pesos:") # Despliega en pantalla los valores de los pesos de la red.
print(red.W)
print("Ganancias:")  # Despliega en pantalla los valores de las ganancias de la red.
print(red.B)

fin = time.time()
print("\n")
print("Tiempo transcurrido:",(fin-ini)) 

print("\n")
print("La salida de la red para la matriz de entrada P (traspuesta) es: ")
print(red.sim(Pt))

print("\n")
print("La salida de la red para los caracteres de simulación es",red.sim(Img_simt)) # Despliega en pantalla la salida de la red para la entrada

