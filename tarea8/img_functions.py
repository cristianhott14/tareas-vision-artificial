import cv2 as cv
from matlab_functions import bwareaopen, bwlabel

def etiq_recort_resc(img_org, caracter):

    #Cambia la dimension de la imagen original a una imagen mas pequeña
    img=cv.resize(img_org, dsize=(494, 300), interpolation=cv.INTER_CUBIC) 

    #Transforma la imagen a escalada de grises
    Agris = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    #Transforma a imagen binaria utilizando el umbral 250
    ret1, Abin1 = cv.threshold(Agris,250,1,0)

    #Filtra la imagen
    Abin2 = bwareaopen(Abin1,10)
    Abin3 = Abin2*255

    #Calcula dimensión de matriz de imagen
    s = Abin2.shape

    # Etiqueta todos los caracteres
    Aetiq = bwlabel(Abin2)

    # Barrido de la imagen para encontrar las filas y columnas iguales a la etiqueta para posteriormente recortar la imagen
    filas=[]
    columnas=[]
    for fil in range (1, s[0] - 1):
        for col in range (1, s[1] - 1):
            #Busca y almacena las filas y columnas donde el pixel sea igual a la etiqueta
            if Aetiq[fil,col] == caracter:
                filas.append(fil)
                columnas.append(col)

    #Se recorta el caracter de la imagen binaria
    img_recortada = (Abin3[min(filas)-3:max(filas)+3,min(columnas)-3:max(columnas)+3]*255)

    #Se reescala la imagen binaria
    img_rescalada=cv.resize(img_recortada, dsize=(12, 12), interpolation=cv.INTER_CUBIC) 

    #print("Imagen rescalada 10x10 \n",img_rescalada*255)
    #cv.imshow("Caracter reescalado",img_rescalada*255)
    #cv.waitKey(0)
    #cv.destroyAllWindows()

    return img_rescalada